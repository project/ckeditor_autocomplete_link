<?php

namespace Drupal\ckeditor_autocomplete_link;

use Drupal\Core\Entity\Element\EntityAutocomplete;

/**
 * Defines the CkeditorEntityAutocompleteHelper.
 */
class CKEditorAutocompleteLinkHelper {

  /**
   * Returns the displayable string corresponding to the given path.
   *
   * In the case where the path corresponds to a node, we return the title of
   * the content and its identifier. In case the path matches the home page
   * then we display <front>.If it is an external link we leave value as it is.
   *
   * @param string $path
   *   Internal path or external url to be analysed.
   *
   * @return string
   *   Corresponding to title of node or url.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getPathAsDisplayableString(string $path): string {

    $url = \Drupal::service('path.validator')->getUrlIfValid($path);

    $displayable_string = $url->toString();

    if ("/" === $path) {
      $displayable_string = "<front>";
    }
    else {
      if (!$url->isExternal()) {
        $scheme = explode('.', $url->getRouteName())[0];
        if ($scheme === 'entity') {
          [$entity_type, $entity_id] = explode('/', substr($path, 0), 2);
          if ($entity_type == 'node' && $entity = \Drupal::entityTypeManager()
            ->getStorage($entity_type)
            ->load($entity_id)) {
            $displayable_string = EntityAutocomplete::getEntityLabels([$entity]);
          }
        }
      }
    }

    return $displayable_string;
  }

}
