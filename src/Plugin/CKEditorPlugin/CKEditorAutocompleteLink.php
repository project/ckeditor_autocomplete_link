<?php

namespace Drupal\ckeditor_autocomplete_link\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\ckeditor\Plugin\CKEditorPlugin\DrupalLink;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\editor\Entity\Editor;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the CkeditorEntityAutocompleteLink plugin.
 */
class CKEditorAutocompleteLink extends DrupalLink implements CKEditorPluginConfigurableInterface, ContainerFactoryPluginInterface {

  /**
   * The entity type manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity type bundle information.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  private EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $bundle_info) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor): array {
    $settings = $editor->getSettings();

    $form['ckeditor_autocomplete_link_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable CKEditor Autocomplete Link ?'),
      '#default_value' => $settings['plugins']['drupallink']['ckeditor_autocomplete_link_enabled'] ?? '',
      '#description' => $this->t('Enabling this field will override the native link field of ckeditor by an entity_autocomplete link.'),
    ];

    $bundle_info = $this->entityTypeBundleInfo->getBundleInfo('node');
    $bundles = [];
    foreach ($bundle_info as $bundle => $info) {
      $bundles[$bundle] = $info['label'];
    }

    $form['ckeditor_autocomplete_link_node'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Content types'),
      '#states' => [
        'visible' => [
          ':input[name="editor[settings][plugins][drupallink][ckeditor_autocomplete_link_enabled]"]' => ['checked' => TRUE],
        ],
        'hidden' => [
          ':input[name="editor[settings][plugins][drupallink][ckeditor_autocomplete_link_enabled]"]' => ['checked' => FALSE],
        ],
      ],
    ];
    $form['ckeditor_autocomplete_link_node']['bundles'] = [
      '#type' => 'checkboxes',
      '#options' => $bundles,
      '#default_value' => $settings['plugins']['drupallink']['ckeditor_autocomplete_link_node']['bundles'] ?? [],
      '#description' => $this->t('Select content types to be available as autocomplete suggestions. If no selected, all will be available.'),
    ];

    return $form;
  }

}
