# CKEditor Autocomplete Link

This module replaces the native ckeditor link field by an entity autocomplete
link field. You can insert internal and external links.

**Currently, autocompletion only supports node entity (most common case).**
**You can create an issue, if you need other entities**


## Features
* The field displays to the user the title and the identifier of the node.
* You can configure the content types used by the autocomplete for each text
format.
* The autocomplete field, allows the user to fill in:
  * A node (displays in the field the title and the identifier of the node)
  * An internal route (displays the path in the field)
  * An external link (displays the url in the field)
  * A token for the homepage (displays in the field the token <front>)


## Benefits
* The user has the same rendering as a link field of content types.
* It is very light (3 files) and does not use custom javascript.
* Less code, also says that upgrades, patches are easier to fix
* **It can be installed at any time on the project because the new field is**
  **only used as an input aid, but it's always the href field that's populated**
* Easy to use, because all parameters are grouped in the text editor interface.


## Requirements
* This module requires that the ckeditor module (core module) be activated


## Installation
1. Install the module in the usual
   way, [see installing contributed modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
2. Enable the module


## Configuration
1. Go to the "Text formats and editors" page (`admin/config/content/formats`)
and select a text format.
2. Make sure the "Link" plugin is added in the CKEditor toolbar.
3. In the "CKEditor plugin settings" section, click on "Drupal link".
4. Enable the field  "Enable CKEditor Entity Autocomplete Link?".
5. Select content types that will be used on the autocomplete field.
6. Save, it's finished


## Recommended modules
* I recommend you to install the [pathologic module](https://www.drupal.org/project/pathologic), then activate its filter which will transform the system paths of your nodes into url aliases and correct potential broken links within your
pages
* Module work with [Editor Advanced Link](https://www.drupal.org/project/editor_advanced_link)


## Comparison others modules
* It is light compared to the [linkit](https://www.drupal.org/project/linkit) module which integrates a profile management and its own autocompletion system. Linkit is more complete, but it alters the rendering of the native autocompletion field. This can disturb some users when using it

* [CKEditor Entity Link](https://www.drupal.org/project/ckeditor_entity_link) adds a new type of plugin instead of overloading the native ckeditor link. It does not have a button to remove the relationship between the text and the link. And the proposed architecture relies on custom forms, which makes it more complicated to maintain in case of ckeditor evolution.


## Roadmap :
Under reflection to support all types of entities.


## Credits
Current maintainer: Marc-Antoine Marty
